import type { NextApiRequest, NextApiResponse } from 'next'

interface nextApiQueryParams extends NextApiRequest {
    query: {
        person: string
    }
}


export default function helloPerson(
  req: nextApiQueryParams,
  res: NextApiResponse
) {
    const person = req.query.person;
    if (!person) {
        return res.status(400).json({message: "must have name as param!"})
    }

  res.status(200).json({ person: `Hello, ${person}` })
}