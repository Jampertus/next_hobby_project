import type { NextPage } from 'next'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { IPerson } from '../../types/IPerson';

import { useSession } from 'next-auth/react'
import { signIn, signOut } from 'next-auth/react'

const HelloPerson: NextPage = () => {

    const [person, setPerson] = useState<IPerson>();

    const { data: session, status } = useSession();
    const loading = status === 'loading';


    useEffect(() => {
      getPerson();  
    }, [session]);

    const getPerson = async () => {
        const response = await fetch("/api/hello/janne");
        const data = await response.json(); 
        setPerson(data.person ? data.person : data.message);
    };

  // If no session exists, display access denied message
  if (!session) { return (
    <>
      <h1>Access Denied</h1>
      <p>
        <a href="/api/auth/signin"
           style={{color: 'blue'}}
           onClick={(e) => {
           e.preventDefault()
           signIn()
        }}>You must be signed in to view this page</a>
      </p>
    </>
  ) }

  return (
    <div>
      <Head>
        <title>Hello</title>
        <meta name="hello" content="Say hello to user" />
      </Head>
      <h1>{person}</h1>
      <p><strong>Welcome {status !== 'loading' && session?.user?.name ? session.user.name : "No user"}</strong></p>
        <a href="/api/auth/signout"
           style={{color: 'blue'}}
           onClick={(e) => {
           e.preventDefault()
           signOut()
        }}>click to sign out</a>
    </div>
  )
}

export default HelloPerson