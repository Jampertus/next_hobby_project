import { GetStaticProps, GetStaticPropsResult, NextPage } from "next";

import { http } from "../../helpers/http";


interface IMessage {
    message: string,
    status: number
}

const mockTest: NextPage<IMessage> = (props) => {

  return (
    <div>
      <p>Message is {props.message}</p>
    </div>
  );
};

export const getStaticProps: GetStaticProps = async (context): Promise<GetStaticPropsResult<IMessage>> => {
  
    let response;
    try {
        const data = await http<IMessage>("http://hopoapi.fi/kissa");
        response = { 
            message: data.message,
            status: 200
        };
    } catch {
        response = {
            message: "Request failed, please try again later",
            status: 500
        };
    }
    

    return {
        props: {
            ...response
        }
  };
};

export default mockTest;
