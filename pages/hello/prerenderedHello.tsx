import { GetStaticProps, GetStaticPropsResult, NextPage } from "next";
import { useSession } from 'next-auth/react'

import { http } from "../../helpers/http";
import { IPerson } from "../../types/IPerson";

const prerederedHello: NextPage<IPerson> = (props) => {

    const { data: session, status } = useSession()

    return (
        <div>
            <h1>{props.person ? props.person : props.message}</h1>
            <p><strong>Welcome {session?.user ? session.user.name : "No user"}</strong></p>
        </div>
    );
};



export const getStaticProps: GetStaticProps = async (): Promise<GetStaticPropsResult<IPerson>> => {
    const data = await http<IPerson>("http://127.0.0.1:3000/api/hello/janne");
    const response: IPerson = data.person ? { person: data.person } : { message: data.message };
    

    return {
        props: {
             ...response,
        },
        revalidate: 15
    }
}

export default prerederedHello