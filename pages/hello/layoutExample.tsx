import { NextPage } from "next";
import { useState } from "react";

import Header from "../../components/Header";
import styles from "../../styles/layout.module.css";

const layoutExample: NextPage = () => {

    const [sideDrawer, setSideDrawer] = useState(false);

    const sideDrawerHandler = () => {
        setSideDrawer(showStatus => !showStatus);
    };

    return (
        <div className={styles.div}>
            <Header sideDrawerHandler={sideDrawerHandler}/>
            <main className={styles.main}>
                {!sideDrawer ?
                    <>
                        <p
                            className={styles.closedSideDrawer}
                        ></p>
                        <div className={styles.content}>
                            <p>Option 1</p>
                            <p>Option 2</p>
                            <p>Option 3</p>
                            <p>Option 4</p>
                            <p>Option 5</p>
                            <p>Option 6</p>
                            <p>Option 7</p>
                            <p>Option 8</p>
                            <p>Option 9</p>
                            <p>Option 10</p>
                            <p>Option 11</p>
                            <p>Option 12</p>
                            <p>Option 13</p>
                        </div>
                    </>
                :
                    <p
                        className={styles.openSideDrawer}
                    >
                        Show sidedrawer: {sideDrawer ? "true" : "false"}
                    </p>
                }               
            </main>
            <footer className={styles.footer}>footer</footer>    
        </div>
    );
  
}

export default layoutExample