import { NextPage } from "next";
import Link from "next/link";

import styles from "../styles/header.module.css";

interface IProps {
    sideDrawerHandler: () => void;
}

const Header: NextPage<IProps> = (props) => {

    return (
        <nav className={styles.nav}>
            <div
                className={styles.div}
                onClick={() => props.sideDrawerHandler()}
            >
                <span />
                <span />
                <span />
            </div>
            <Link  href="/">
                <a>default index</a>    
            </Link>
            <Link href="/hello">
                <a>Hello</a>    
            </Link>
        </nav>
    );  
}

export default Header;