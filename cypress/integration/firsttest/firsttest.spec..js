import { When, Then } from 'cypress-cucumber-preprocessor/steps';

When('I visit the layout page', function () {
    cy.visit('http://localhost:3000/hello/layoutExample');
});
Then('I should see {string}', function (text) {
    cy.contains('Option 1');
});

// describe("First test", () => {
//     it("visits hello/layoutExample", () => {
//         cy.visit('http://localhost:3000/hello/layoutExample');
//     })

//     it("Contains text Option 1", () => {
//         cy.contains('Option 1');
//     })
// })